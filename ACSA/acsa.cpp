#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <sys/time.h>
#include <unistd.h>

#include <iostream>
#include <sstream>

#include "Header.h"

#ifndef PI
    #define PI 3.14159265358979323846264338327
#endif

#define MAXITER 3000000
#define TGEN 0.1
#define DIMENSION 1000

struct SharedCacheLine{
    double cost; // 8 bytes
    char padding[56]; // fill up cache line -> 7 bytes 
};

struct SharedBestCosts{
    double cost;
    char padding[56];
};

int main( int argc, const char* argv[] ){

	struct timeval start, end;
	long seconds, useconds;    
	double mtime;
	gettimeofday(&start, NULL);    
		
	 
	int iter=0,maxIter = MAXITER, dim = atoi(argv[3]), numOpt = atoi(argv[1]) , numFun=atoi(argv[2]);

	double tgen=TGEN,tac=0.9,gamma =0.0;
	struct SharedCacheLine *sharedValues   = (struct SharedCacheLine *) malloc(numOpt*sizeof(struct SharedCacheLine));
	struct SharedBestCosts *sharedBestCost = (struct SharedBestCosts *) malloc(numOpt*sizeof(struct SharedBestCosts));

	omp_lock_t lock_tac;
	omp_init_lock(&lock_tac); //Lock

	double maxCost = 0.0;

	#pragma omp parallel shared(maxCost,iter,lock_tac,tgen,tac,gamma,sharedValues,sharedBestCost,end) firstprivate(maxIter,numFun,dim,numOpt) num_threads(numOpt)  default(none)// reduction(+ : nsum)
	{

		int k,j,optId = omp_get_thread_num();
		double *curSol,*temp,*sol;
		struct drand48_data buffer;
		double currCost,bestcost,tmp,prob,probVar,result,cost=0.0;
		Benchmarks *fp = generateFuncObj(numFun);
		
		fp->setDimension(dim); //set the dimension of the problem
			
		curSol = (double *) malloc(dim*sizeof(double)); //current solution
		sol = (double *) malloc(dim*sizeof(double));	//probing solution

		srand48_r(time(NULL),&buffer); //seed

		for(j=0;j<dim;j++){
			drand48_r(&buffer, &result);
			curSol[j] = fp->getMaxX()*(result); //Do prof
		}
		bestcost = currCost = sharedValues[optId].cost = fp->compute(curSol); //evaluate objective function
		
		#pragma omp single
		{
				maxCost = sharedValues[0].cost;
				for (k=1; k<numOpt; k++){
					if (sharedValues[k].cost>maxCost)
						maxCost = sharedValues[k].cost;
				}
		} //Implicit Barrier
				
		#pragma omp critical
		{
			gamma += exp((sharedValues[optId].cost - maxCost)/tac);

		} // No implicit barrier
		#pragma omp barrier

			while (iter < maxIter){

				//Probing Solution
				for(j=0;j<dim;j++){
					drand48_r(&buffer, &result); 
					result = tan(PI*(result-0.5));
					sol[j]=fmod(curSol[j]+(tgen*result*fp->getMaxX() ) , fp->getMaxX() );
				}
				//Cost of probing solution
				cost = fp->compute(sol);

				if( cost < currCost ){
					
					currCost = cost;
					if (omp_test_lock(&lock_tac)){
						sharedValues[optId].cost = currCost;
					  omp_unset_lock(&lock_tac);
					}
					temp = sol; sol = curSol; curSol = temp; //do solution <- probing solution by changing pointers

					if (currCost < bestcost){
						bestcost = cost; //save the best solution 
					} 
				} else { 
					drand48_r(&buffer, &result);
					prob = exp((sharedValues[optId].cost - maxCost)/tac)/gamma;

					if( prob > result ){
						currCost = cost; //update the cost of current solution
						if (omp_test_lock(&lock_tac)){
							sharedValues[optId].cost = cost;
							omp_unset_lock(&lock_tac);
						}
						temp = sol; sol = curSol; curSol = temp; //do solution <- probing solution by changing pointers
					}
				}

				if (omp_test_lock(&lock_tac)){ //try lock 

					tmp = sharedValues[0].cost;
					for (k=1; k<numOpt; k++){
						if (sharedValues[k].cost>maxCost){
							tmp = sharedValues[k].cost;
						}
					}
					maxCost = tmp;

					gamma = tmp = 0.0;
					for (k=0; k<numOpt; k++){
						gamma += exp((sharedValues[k].cost - maxCost)/tac);
						tmp += exp(2.0*(sharedValues[k].cost - maxCost)/tac);
					}

					tmp = tmp/(gamma*gamma);
					probVar = (tmp*((double)(numOpt))-1.0)/((double)numOpt-1.0);

					if (probVar >= 0.99) 
						tac += 0.05*tac;
					else
						tac -= 0.05*tac;

					tgen = 0.99999*tgen; //update the generating temperature

					#pragma omp flush(maxCost,iter,tgen,tac,gamma,sharedValues)

					omp_unset_lock(&lock_tac);
				}

				#pragma omp atomic
				++iter; //increment interations

			} // End of main loop
		   
		#pragma omp barrier
		#pragma omp single
		{
			gettimeofday(&end, NULL); //save the time
		}
		
		free(curSol);
		free(sol);

		//Save each individual bestcost in sharedBestCost
		sharedBestCost[optId].cost = bestcost;

	} // omp parallel - Implicit Barrier

	//Calculating time
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = (((seconds) * 1000 + useconds/1000.0) + 0.5)/1000;

	free(sharedValues);
	free(sharedBestCost);
	omp_destroy_lock(&lock_tac);

	//Looking for BestCost in sharedBestCost
	double bestcost = sharedBestCost[0].cost;
	for (int k=1; k<numOpt; k++)
		if (sharedBestCost[k].cost < bestcost)
				bestcost = sharedBestCost[k].cost;
	printf("f%i bestcost: %2.3e, time=%2.3e \n\n", numFun,bestcost, mtime);	
	
	return(0);
}

// create new object of class with default setting
Benchmarks* generateFuncObj(int funcID){
	Benchmarks *fp;

	// run each of specified function in "configure.ini"
	if (funcID==1){
			fp = new F1();
	}else if (funcID==2){
			fp = new F2();
	}else if (funcID==3){
			fp = new F3();
	}else if (funcID==4){
			fp = new F4();
	}else if (funcID==5){
			fp = new F5();
	}else if (funcID==6){
			fp = new F6();
	}else if (funcID==7){
			fp = new F7();
	}else if (funcID==8){
			fp = new F8();
	}else if (funcID==9){
			fp = new F9();
	}else if (funcID==10){
			fp = new F10();
	}else if (funcID==11){
			fp = new F11();
	}else if (funcID==12){
			fp = new F12();
	}else if (funcID==13){
			fp = new F13();
	}else if (funcID==14){
			fp = new F14();
	}else if (funcID==15){
			fp = new F15();
	}else if (funcID==16){
			fp = new F16();
	}else if (funcID==17){
			fp = new F17();
	}else if (funcID==18){
			fp = new F18();
	}else if (funcID==19){
			fp = new F19();
	}else if (funcID==20){
			fp = new F20();
	}else{
			cerr<<"Fail to locate Specified Function Index"<<endl;
			exit(-1);
	}
	return fp;
}

